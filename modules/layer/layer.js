/*
 @author      Holger Schauf
 @title       Layer Elements
 @type        module
 @description generate layer with defined content

 @example     var layer = new mote.Layer(); layer.open();
 @example     mote.run('Layer').open();
 */

mote.register('Layer');

mote.Layer = (function ($, tmpl) {
    return Class.extend({

        cfg: {
            name: "lyr",
            close: {
                isActive: true,
                name: 'close-lyr',
                position: 'top-rgt',
                classes: null,
                adjustTo: null
            },
            animate: {
                show: false,
                hide: false
            },
            dropArea: 'body',
            classes: 'lyr-fxd bgt-wht-80 zdx-1 cnr',
            content: {
                name: '.content',
                classes: 'pdg-',
                adjustTo: null
            },
            closeCallback: false,
            trigger: null
        },

        isActive: false,

        /**
         *
         * @param cfg
         * @returns {boolean}
         */
        init: function (cfg) {
            this.cfg    = $.extend({}, this.cfg, cfg);
            this.cfg.id = this.cfg.name + mote.generateUID();

            this.setToggle();

            return this.generate();
        },

        setToggle: function () {

            if (this.cfg.trigger === null) return false;

            this.toggle = mote.run('Toggle', {
                trigger: this.cfg.trigger,
                target: this.elm
            });
        },

        /**
         *
         * @returns {boolean}
         */
        generate: function () {

            if (this.cfg.close.isActive)
                this.cfg.classes += " closable";

            var response = mote.getTemplate(this.cfg.name, this.cfg);

            if (!response) return false;

            $(this.cfg.dropArea).append(response);

            this.elm = $('#' + this.cfg.id);
            this.content = this.elm.find(this.cfg.content.name);

            if (this.cfg.close.isActive)
                this.elm.bind('click.' + this.cfg.name, mote.doLater(this.close, this));

            return true;

        },

        /**
         *
         * @param content
         * @returns {boolean}
         */
        setContent: function (content) {

            if (this.cfg.close.isActive) {
                var response = mote.getTemplate(this.cfg.name + 'Close', this.cfg);

                if (!response) return false;

                content = $(content).prepend(response);
            }

            this.elm.find(this.cfg.content.name).html(content);

            this.adjustContent();
            this.setCloseEvents();

            return true;
        },

        /**
         *
         * @returns {boolean}
         */
        adjustContent: function () {
            var adjustToElm = $(this.cfg.content.adjustTo);

            // stop on following behaviors
            if (this.cfg.content.adjust === null && adjustToElm.length === 0) return false;

            this.content.css({
                marginTop: (adjustToElm.offset().top + adjustToElm.outerHeight(true))
            });

            return true;
        },

        /**
         *
         * @returns {boolean}
         */
        setCloseEvents: function () {

            var closeElements = this.elm.find('.' + this.cfg.close.name);

            if (closeElements.length > 0)
                closeElements.off().on("click", mote.doLater(this.close, this, true));

            return closeElements.length > 0;
        },

        open: function () {
            /* check for new content */
            if (arguments.length > 0)
                this.setContent(arguments[0]);

            this.setLevel();

            if (this.cfg.animate.open !== false) {
                //mote.run('Animate', this.elm, )
            }

            this.elm.show();

            this.isActive = true;

            /* check for callback */
            if (arguments.length > 1)
                arguments[1]();
        },

        close: function () {
            if (
                arguments.length > 0
                    && (typeof arguments[0] === 'boolean' && arguments[0] === true)
                    || (this.cfg.id === arguments[0].target.id || $(arguments[0].target).hasClass("close"))
                ) {
                this.elm.hide();
                this.isActive = false;
                if (this.cfg.closeCallback !== false && typeof this.cfg.closeCallback === "function") {
                    this.cfg.closeCallback();
                }
            }
        },

        setLevel: function () {
            var instances = mote.app('Layer'),
                level = 0, i;

            for (i = 0; i < instances.length; i++)
                if (instances[i].isActive) level++;

            this.elm.zIndex(level.length);
        }

    });
})(jQuery, $.templates);