### install brew
ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
### brew install wget and node
brew install wget
brew install node
### node package manager install grunt
npm install -g bower
npm install jshint
npm install -g grunt-cli
npm install grunt --save-dev
npm install grunt-contrib-csslint --save-dev
npm install grunt-contrib-concat --save-dev
npm install grunt-contrib-uglify --save-dev
npm install grunt-contrib-cssmin --save-dev
npm install grunt-contrib-qunit --save-dev
npm install grunt-contrib-watch --save-dev
npm install grunt-filerev --save-dev
npm install grunt-contrib-jshint --save-dev